import csv
import json

def lookup_failed_slots(queryString):
    
    queryReturn = {}
    
    with open('C:/Users/Hoggit/Saved Games/DCS.openbeta_server/failed-slots.txt', 'r', encoding="utf-8") as handle:
        reader = csv.reader(handle)   
        failedSlots_dict = {rows[0]:rows[1] for rows in reader}
    handle.close()

    for ucid in failedSlots_dict:
        if queryString.lower() in failedSlots_dict[ucid].lower():
            queryReturn[ucid] = failedSlots_dict[ucid]

    return json.dumps(queryReturn)


def write_to_whitelist(ucid, name, discordtag):

    with open('C:/Users/Hoggit/Saved Games/DCS.openbeta_server/whitelist-clients.lua', 'r', encoding="utf-8") as handle:
        whitelistLines = handle.readlines()
    handle.close()

    whitelistLines.insert(len(whitelistLines)-1, ('    \'' + ucid + '\', -- ' + name + ', ' + discordtag + '\n'))

    with open('C:/Users/Hoggit/Saved Games/DCS.openbeta_server/whitelist-clients.lua', 'w', encoding="utf-8") as handle:
        handle.writelines(whitelistLines)
    handle.close()
