import json
import datetime
import lupa
import os

STATS_PATH = "./data/slmodstats.json"


def get_data():
    if not os.stat(STATS_PATH).st_size:
        return {}
    with open(STATS_PATH, 'r') as handle:
        return json.load(handle)


def find_user_by_name(username, match):
    data = get_data()
    users = []

    for uID in data:
        for field in data[uID]:
            if field == 'names':
                for i in data[uID][field]:
                    if match:
                        if data[uID][field][i].lower() == username.lower():
                            users.append(uID)
                    else:
                        if username.lower() in data[uID][field][i].lower():
                            users.append(uID)

    return {user: data[user] for user in users}


def find_user_by_uid(ucid):
    data = get_data()
    if ucid not in data:
        return {}

    return {ucid: data[ucid]}


def jsonformat(stats_dict):

    for uID in stats_dict:
        for field in stats_dict[uID]:

            if field == 'joinDate':
                try:
                    stats_dict[uID][field] = datetime.datetime.fromtimestamp(int(stats_dict[uID][field]), tz=datetime.timezone.utc).isoformat()
                except TypeError:
                    pass

            if field == 'lastJoin':
                try:
                    stats_dict[uID][field] = datetime.datetime.fromtimestamp(int(stats_dict[uID][field]), tz=datetime.timezone.utc).isoformat()
                except TypeError:
                    pass

            if field == 'friendlyKills' or field == 'friendlyCollisionKills':
                for kill_number in stats_dict[uID][field]:
                    try:
                        stats_dict[uID][field][kill_number]['time'] = datetime.datetime.fromtimestamp(
                            int(stats_dict[uID][field][kill_number]['time']), tz=datetime.timezone.utc).isoformat()
                    except TypeError:
                        pass

    return stats_dict


def addbaninfo(stats_dict):

    lua = lupa.LuaRuntime()

    with open('./data/BannedClients.lua', mode='r', encoding="utf-8") as banlua:
        ban_file = banlua.read()

    lua.execute(ban_file)
    banned_ips = lua.globals().slmod_banned_ips
    banned_uids = lua.globals().slmod_banned_ucids

    ip_bans = []
    uid_bans = []

    for header, table in banned_uids.items():
        uid_bans.append(header)

    for ip, details in banned_ips.items():
        for ucid, uID in details.items():
            if ucid == 'ucid':
                ip_bans.append(uID)

    for uID in stats_dict:
        if uID in uid_bans:
            stats_dict[uID]['uIDbanned'] = True

        if uID in ip_bans:
            stats_dict[uID]['IPbanned'] = True
    return stats_dict


def addAdminInfo(stats_dict):

    lua = lupa.LuaRuntime()

    with open('./data/ServerAdmins.lua', mode='r', encoding="utf-8") as adminlua:
        admin_file = adminlua.read()
    
    lua.execute(admin_file)
    admins = []

    for ucid, name in lua.globals().Admins.items():

        admins.append(ucid)

    for uID in stats_dict:
        if uID in admins:

            stats_dict[uID]['admin'] = True

    return(stats_dict)


def addbaninfoip(ip_dict):

    lua = lupa.LuaRuntime()

    with open('./data/BannedClients.lua', mode='r', encoding="utf-8") as banlua:
        ban_file = banlua.read()

    lua.execute(ban_file)
    banned_ips = lua.globals().slmod_banned_ips

    ip_bans = []

    for header, table in banned_ips.items():
        ip_bans.append(header)

    for ip, trash in ip_dict.items():
        for current_ip in ip_bans:
            if ip in current_ip:
                ip_dict[ip]['IPbanned'] = True
    return ip_dict


def usernamequery(search_str, match):

    query_return = jsonformat(addbaninfo(addAdminInfo(find_user_by_name(search_str, match))))

    return json.dumps(query_return)


def ucidquery(ucid):

    query_return = jsonformat(addbaninfo(addAdminInfo(find_user_by_uid(ucid))))

    return json.dumps(query_return)


def banqueryucid(ucid):

    query_return = addbaninfo(ucid)

    return json.dumps(query_return)


def banqueryip(ip):

    query_return = addbaninfoip(ip)

    return json.dumps(query_return)
