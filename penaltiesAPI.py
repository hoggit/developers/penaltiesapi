import argparse
import os
import logging
import time
import lookup.fullstatslookup as stats
import lookup.whitelist as wl
import jwt
from pathlib import Path
from flask import Flask, request
from slmodstatsparser import parser
from functools import wraps

app = Flask(__name__)
app.config['SECRET_KEY'] = os.environ['API_SECRET_KEY']
logging.basicConfig(level=logging.INFO, filename='./data/log.txt', filemode='w', format='%(asctime)s - %(message)s')


def authenticate(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args.get('token')
        if not token:
            return app.response_class(response='Invalid authentication.', status=401)

        try:
            data = jwt.decode(token, os.environ['API_SECRET_KEY'])
        except:
            return app.response_class(response='Invalid authentication.', status=401)
        return f(*args, **kwargs)
    return decorated


@app.route('/stats/username')
@authenticate
def username():
    logging.info('Incoming Request from IP: ' + str(request.remote_addr))
    logging.info(request)

    print('Incoming Request from IP: ' + str(request.remote_addr))
    print(request)

    user = request.args.get('user', default='Blackbird')
    match = request.args.get('match', default='True')

    if match.lower() == 'true':
        m = True
    else:
        m = False

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-f", "--statsfile",
                            default=(str(Path(__file__).absolute().parent.parent) + '\\SlmodStats.lua'))
    arg_parser.add_argument('-p', '--penfile',
                            default=(str(Path(__file__).absolute().parent.parent) + '\\SlmodPenaltyStats.lua'))
    arg_parser.add_argument('-b',
                            '--banfile',
                            default=(str(Path(__file__).absolute().parent.parent) + '\\BannedClients.lua'))
    arg_parser.add_argument('-a',
                            '--adminfile',
                            default=(str(Path(__file__).absolute().parent.parent) + '\\ServerAdmins.lua'))
    arg_parser.add_argument("-t", "--type",
                            action='append',
                            default='json',
                            choices=['csv', 'json', 'postgres'])
    arg_parser.add_argument("-o", "--outpath",
                            default=(str(Path(__file__).absolute().parent) + '\\data\\'))
    args = arg_parser.parse_args()
    try:
        parser.parse(args)
    except parser.OperationalError:
        time.sleep(10)
        parser.parse(args)

    r = stats.usernamequery(user, m)

    if r == '{}':
        result = app.response_class(
            response=r,
            status=204,
            mimetype='text/plain'
        )
    else:
        result = app.response_class(
            response=r,
            status=200,
            mimetype='application/json'
        )
    return result


@app.route('/stats/ucid')
@authenticate
def ucid():

    logging.info('Incoming Request from IP: ' + str(request.remote_addr))
    logging.info(request)

    print('Incoming Request from IP: ' + str(request.remote_addr))
    print(request)

    uid = request.args.get('user', default='')

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-f", "--statsfile",
                            default=(str(Path(__file__).absolute().parent.parent) + '\\SlmodStats.lua'))
    arg_parser.add_argument('-p', '--penfile',
                            default=(str(Path(__file__).absolute().parent.parent) + '\\SlmodPenaltyStats.lua'))
    arg_parser.add_argument('-b',
                            '--banfile',
                            default=(str(Path(__file__).absolute().parent.parent) + '\\BannedClients.lua'))
    arg_parser.add_argument('-a',
                            '--adminfile',
                            default=(str(Path(__file__).absolute().parent.parent) + '\\ServerAdmins.lua'))
    arg_parser.add_argument("-t", "--type",
                            action='append',
                            default='json',
                            choices=['csv', 'json', 'postgres'])
    arg_parser.add_argument("-o", "--outpath",
                            default=(str(Path(__file__).absolute().parent) + '\\data\\'))
    args = arg_parser.parse_args()
    try:
        parser.parse(args)
    except parser.OperationalError:
        time.sleep(10)
        parser.parse(args)

    r = stats.ucidquery(uid)

    if r == '{}':
        result = app.response_class(
            response=r,
            status=204,
            mimetype='text/plain'
        )
    else:
        result = app.response_class(
            response=r,
            status=200,
            mimetype='application/json'
        )
    return result


@app.route('/bans/ucid')
@authenticate
def banucid():

    logging.info('Incoming Request from IP: ' + str(request.remote_addr))
    logging.info(request)

    print('Incoming Request from IP: ' + str(request.remote_addr))
    print(request)

    uid = request.args.get('user', default='')

    qdict = {uid: {}}

    r = stats.banqueryucid(qdict)

    if r == '{}':
        result = app.response_class(
            response=r,
            status=204,
            mimetype='text/plain'
        )
    else:
        result = app.response_class(
            response=r,
            status=200,
            mimetype='application/json'
        )
    return result


@app.route('/bans/ip')
@authenticate
def banip():

    logging.info('Incoming Request from IP: ' + str(request.remote_addr))
    logging.info(request)

    print('Incoming Request from IP: ' + str(request.remote_addr))
    print(request)

    ip = request.args.get('user', default='')

    qdict = {ip: {}}

    r = stats.banqueryip(qdict)

    if r == '{}':
        result = app.response_class(
            response=r,
            status=204,
            mimetype='text/plain'
        )
    else:
        result = app.response_class(
            response=r,
            status=200,
            mimetype='application/json'
        )
    return result

@app.route('/failedslots')
@authenticate
def failedslots():

    logging.info('Incoming Request from IP: ' + str(request.remote_addr))
    logging.info(request)

    print('Incoming Request from IP: ' + str(request.remote_addr))
    print(request)

    user = request.args.get('user', default='')

    r = wl.lookup_failed_slots(user)

    if r == '{}':
        result = app.response_class(
            response=r,
            status=204,
            mimetype='text/plain'
        )
    else:
        result = app.response_class(
            response=r,
            status=200,
            mimetype='application/json'
        )
    return result

@app.route('/whitelist')
@authenticate
def whitelist():

    logging.info('Incoming Request from IP: ' + str(request.remote_addr))
    logging.info(request)

    print('Incoming Request from IP: ' + str(request.remote_addr))
    print(request)

    ucid = request.args.get('ucid', default='')
    user = request.args.get('user', default='')
    discord = request.args.get('discord', default='')

    wl.write_to_whitelist(ucid, user, discord)

    return app.response_class(response='Whitelisted user succersfully', status=200, mimetype='text/plain')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False, ssl_context=('fullchain.pem', 'privkey.pem'))
    #app.run(host='', port=5000, debug=True)
