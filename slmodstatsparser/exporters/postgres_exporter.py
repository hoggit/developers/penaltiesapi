from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, ForeignKey, DateTime, create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.dialects.postgresql import JSONB
from slmodstatsparser.exporters import lua2python
from uuid import uuid4
import datetime
import os


def parse(data, pendata, outpath):
    db_host = os.environ.get("PG_HOST", "localhost")
    db_user = os.environ.get("PG_USER", "postgres")
    db_pass = os.environ.get("PG_PASS", "admin")
    db_name = os.environ.get("PG_DB", "postgres")
    db_port = os.environ.get("PG_PORT", "5432")
    server = os.environ.get("SERVER", "GAW")

    base = declarative_base()

    class Player(base):
        __tablename__ = 'players'
        uid = Column(String, primary_key=True)
        stat_documents = relationship('Stats', backref="player")

    class PlayerNames(base):
        __tablename__ = 'players_names'
        player_id = Column(String, ForeignKey('players.uid'), primary_key=True)
        name = Column(String, nullable=False, primary_key=True)

    class Stats(base):
        __tablename__ = 'stats'
        id = Column(String, primary_key=True)
        server = Column(String, primary_key=True)
        player_id = Column(String, ForeignKey('players.uid'))
        datetime = Column(DateTime, default=datetime.datetime.utcnow)
        stats = Column(JSONB)

    engine = create_engine("postgresql://{0}:{1}@{2}:{4}/{3}".format(db_user, db_pass, db_host, db_name, db_port))
    session = sessionmaker()
    session.configure(bind=engine)
    base.metadata.create_all(engine)

    db_session = session()
    try:
        import sys
        data = lua2python(data, pendata)
        for uid, stat in data.items():
            db_session.merge(Player(uid=uid))
            db_session.add(Stats(id=str(uuid4()), player_id=uid, stats=stat, server=server))
            for name in stat['names']:
                db_session.merge(PlayerNames(player_id=uid, name=name))
        db_session.commit()
    finally:
        db_session.close()
