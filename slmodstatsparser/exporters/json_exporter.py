import json
from slmodstatsparser.exporters import lua2python


def parse(data, pendata, outpath):
    with open('{0}/slmodstats.json'.format(outpath), 'w') as f:
        json.dump(lua2python(data, pendata), f)
    f.close()
