def lua2python(data, pendata):
    export_data = {}
    for uID, stat in data.items():
        export_data[uID] = {}
        export_data[uID]['slID'] = stat.id
        export_data[uID]['joinDate'] = stat.joinDate
        export_data[uID]['lastJoin'] = stat.lastJoin

        for aircraftID, aircraftData in stat.times.items():
            export_data[uID][aircraftID] = {}
            for category, sublist in aircraftData.items():
                if category == 'total':
                    export_data[uID][aircraftID]['time' + category] = round(aircraftData.total, 0)
                elif category == 'inAir':
                    export_data[uID][aircraftID]['time' + category] = round(aircraftData.inAir, 0)
                else:
                    export_data[uID][aircraftID][category] = {}
                    for subset, actions in sublist.items():
                        export_data[uID][aircraftID][category][subset] = {}
                        #for action, value in actions.items():
                            #export_data[uID][aircraftID][category][subset][action] = value
        export_data[uID]['names'] = {}
        for number, name in stat.names.items():
            export_data[uID]['names'][number] = name

    for uID, pens in pendata.items():
        if uID not in export_data:
            names = {number: name for number, name in pens.names.items()}
            export_data[uID] = {
                "slID": pens.id,
                "joinDate": pens.joinDate,
                "names": names,
            }
        for cat, num in pens.items():
            if cat == 'friendlyKills' or cat == 'friendlyCollisionKills':
                export_data[uID][cat] = {}
                for types, value in num.items():
                    export_data[uID][cat][types] = {}
                    for action, amount in value.items():
                        if action == 'human':
                            export_data[uID][cat][types][action] = {}
                            if isinstance(amount, str):
                                export_data[uID][cat][types][action]['1'] = amount
                            else:
                                for a, b in amount.items():
                                    export_data[uID][cat][types][action][a] = b
                        else:
                            export_data[uID][cat][types][action] = amount

    return export_data
