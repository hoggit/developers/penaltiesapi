import csv
from slmodstatsparser.exporters import lua2python


def write_result(path, headers, data):
    start_columns = ["uID", "slID", 'joinDate', 'lastJoin']
    headers = sorted(headers.difference(set(start_columns)))
    # put the keys at the beginning
    for pos, key in enumerate(start_columns):
        headers.insert(pos, key)
    # write the CSV, injecting 0 for any missing value
    with open(path, 'w', newline='', encoding='utf-8') as df:
        writer = csv.DictWriter(df, fieldnames=headers)
        writer.writeheader()
        for p in data:
            for k in headers:
                if k not in p:
                    p[k] = 0
            writer.writerow(p)


def parse(statsdata, pendata, outpath):

    statsdata = lua2python(statsdata, pendata)

    header_set = set()
    player_data = []
    tk_header_set = set()
    tk_data = []
    name_header_set = set()
    name_data = []

    for uID, stat in statsdata.items():
        base = {
            "uID": uID,
            "slID": stat['slID'],
            'joinDate': stat['joinDate'],
            'lastJoin': stat['lastJoin']
        }

        current_player = base.copy()
        current_tk = base.copy()
        current_name = base.copy()

        if isinstance(stat, dict):
            for category, types in stat.items():
                if category == "names":
                    if isinstance(types, dict):
                        for subtypes, field in types.items():
                            current_name[category + str(subtypes)] = field
                            current_tk[category + str(subtypes)] = field

                if category == "friendlyKills":
                    if isinstance(types, dict):
                        for subtypes, field in types.items():
                            if isinstance(field, dict):
                                for key, value in field.items():
                                    if isinstance(value, dict):
                                        for x, y in value.items():
                                            current_tk[category + str(subtypes) + str(key) + str(x)] = y
                                    else:
                                        current_tk[category + str(subtypes) + str(key)] = value
                            else:
                                current_tk[category + str(subtypes)] = field

                elif isinstance(types, dict):
                    for subtypes, field in types.items():
                        if isinstance(field, dict):
                            for key, value in field.items():
                                if isinstance(value, dict):
                                    for x, y in value.items():
                                        current_player[category + subtypes + key + x] = y
                                else:
                                    current_player[category + str(subtypes) + key] = value
                        else:
                            current_player[category + str(subtypes)] = field

        header_set.update(current_player)
        player_data.append(current_player)

        tk_header_set.update(current_tk)
        tk_data.append(current_tk)

        name_header_set.update(current_name)
        name_data.append(current_name)

    # Writing the data lists to a csv file
    write_result(outpath + '/player_data.csv', header_set, player_data)

    write_result(outpath + '/tk_data.csv', tk_header_set, tk_data)

    write_result(outpath + '/names_data.csv', name_header_set, name_data)
