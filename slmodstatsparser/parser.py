import argparse
import lupa
import os
import timeit
import time
from shutil import copy
from pathlib import Path
from sqlalchemy.exc import OperationalError
from slmodstatsparser.exporters.csv_exporter import parse as parse_csv
from slmodstatsparser.exporters.json_exporter import parse as parse_json
from slmodstatsparser.exporters.postgres_exporter import parse as parse_postgres


def parse(parse_args):

    # start = timeit.default_timer()

    lua = lupa.LuaRuntime()
    lua2 = lupa.LuaRuntime()

    os.makedirs(parse_args.outpath, exist_ok=True)  # create the output dir if it doesn't exist

    try:
        last_parse = time.time() - os.path.getmtime('./data/slmodstats.json')
    except OSError:
        last_parse = 10000

    if last_parse >= 300 or os.stat('./data/slmodstats.json').st_size == 0:

        try:
            copy(parse_args.statsfile, parse_args.outpath)
            os.remove(parse_args.outpath + 'SlmodStats.lua')
        except OSError:
            print('File handling error.')
        try:
            os.remove(parse_args.outpath + 'SlmodStats.json')
        except OSError:
            print('File handling error.')
        try:
            copy(parse_args.penfile, parse_args.outpath)
            os.remove(parse_args.outpath + 'SlmodPenaltyStats.lua')
        except OSError:
            print('File handling error.')
        try:
            copy(parse_args.banfile, parse_args.outpath)
            os.remove(parse_args.outpath + 'BannedClients.lua')
        except OSError:
            print('File handling error.')
        try:
            copy(parse_args.adminfile, parse_args.outpath)
            os.remove(parse_args.outpath + 'ServerAdmins.lua')
        except OSError:
            print('File handling error.')

        copy(parse_args.statsfile, parse_args.outpath)
        copy(parse_args.penfile, parse_args.outpath)
        copy(parse_args.banfile, parse_args.outpath)
        copy(parse_args.adminfile, parse_args.outpath)

        with open(parse_args.outpath + 'SlmodStats.lua', mode='r', encoding="utf-8") as handle:
            input_file = handle.read()

        lua.execute(input_file)
        statsdata = lua.globals().stats

        with open(parse_args.outpath + 'SlmodPenaltyStats.lua', mode='r', encoding="utf-8") as handle2:
            input_penfile = handle2.read()

        lua2.execute(input_penfile)
        pendata = lua2.globals().penStats

        if 'csv' in parse_args.type:
            parse_csv(statsdata, pendata, parse_args.outpath)
        if 'json' in parse_args.type:
            parse_json(statsdata, pendata, parse_args.outpath)
        if 'postgres' in parse_args.type:
            parse_postgres(statsdata, pendata, parse_args.outpath)


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-f", "--statsfile",
                            required=True,
                            default=(str(Path(__file__).absolute().parent.parent) + '\\SlmodStats.lua'))
    arg_parser.add_argument('-p',
                            '--penfile',
                            required=True,
                            default=(str(Path(__file__).absolute().parent.parent) + '\\SlmodPenaltyStats.lua'))
    arg_parser.add_argument('-b',
                            '--banfile',
                            required=True,
                            default=(str(Path(__file__).absolute().parent.parent) + '\\BannedClients.lua'))
    arg_parser.add_argument('-a',
                            '--adminfile',
                            required=True,
                            default=(str(Path(__file__).absolute().parent.parent) + '\\ServerAdmins.lua'))
    arg_parser.add_argument("-t", "--type",
                            action='append',
                            required=True,
                            choices=['csv', 'json', 'postgres'])
    arg_parser.add_argument("-o", "--outpath",
                            default=(str(Path(__file__).absolute().parent) + '\\data\\'))
    args = arg_parser.parse_args()
    try:
        parse(args)
    except OperationalError:
        time.sleep(10)
        parse(args)
